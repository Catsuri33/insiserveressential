package fr.catsuri33.insiserveressential.lang;

public enum Messages {

    PREFIX("§bInsiServer§fEssential §6» ");



    private final String message;

    Messages(String message){

        this.message = message;

    }

    public String getMessage() {

        return message;

    }

}
